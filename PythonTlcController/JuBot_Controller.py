import asyncio
from time import sleep

from bleak import BleakClient, BleakScanner



# (delay, PWM_array) - will be executed sequentially
tlc_commands = [
    (2, "FF 00 00 00 00 00 00 00"),
    (2, "00 FF 00 00 00 00 00 00"),
    (2, "00 00 FF 00 00 00 00 00"),
    (2, "00 00 00 FF 00 00 00 00"),
    (2, "00 00 00 00 FF 00 00 00"),
    (2, "00 00 00 00 00 FF 00 00"),
    (2, "00 00 00 00 00 00 FF 00"),
    (2, "00 00 00 00 00 00 00 FF")    
]

# name of device to connect to - chose from: {JuBot_TPU, JuBot}
wanted_name = "JuBot_TPU"


ble_address = None
#Event which is set when an correct device was found
found_event = asyncio.Event()

CHARACTERISTIC_UUID = ( "713d0003-503e-4c75-ba94-3148f18d941e")
# Callback which is used to check if a device found by scan has its properties changed
def detection_callback(device, advertisement_data):
    global ble_address
    print(device)
    if device.name == wanted_name:    
        ble_address = device.address
        found_event.set()

async def find_matching_devices_by_name():
    
    scanner = BleakScanner()
    scanner.register_detection_callback(detection_callback)
    await scanner.start()
    await found_event.wait()
    await scanner.stop()



async def updateTLC():
    # connect first
    await find_matching_devices_by_name()

    client = BleakClient(ble_address)
    await client.connect()

    # write tlc command from list, wait for delay, and iterate to next one until finished
    for i in range(len(tlc_commands)):
        await client.write_gatt_char(CHARACTERISTIC_UUID, bytearray.fromhex(tlc_commands[i][1]))
        print("Executing: ", tlc_commands[i])
        sleep(tlc_commands[i][0])
    # turn off all motors
    await client.write_gatt_char(CHARACTERISTIC_UUID, bytearray.fromhex("00 00 00 00 00 00 00 00"))
    print("Turning all motors off and finish!")
    sleep(10)
    await client.disconnect()



asyncio.run(updateTLC())

