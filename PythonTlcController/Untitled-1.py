import asyncio
from time import sleep

from bleak import BleakClient, BleakScanner
wanted_name = "JuBot_TPU"

tlc_commands = [
    (5, "FF FF 00 AA 00 00 00 00"),
    (5, "00 00 00 00 00 00 00 00"),
    (1, "FF FF 00 AA 00 00 00 00")
]


print(tlc_commands[0][1])


async def main(wanted_name):
    device = await BleakScanner.find_device_by_filter(
        lambda d, ad: d.name and d.name.lower() == wanted_name.lower()
    )
    global dev
    dev = device
    print(dev)