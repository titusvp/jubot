/*struct Thread {
  unsigned long executionInterval;
  unsigned long lastExecutionEnded;
  void (*task)();
  
};
*/
/*
#ifdef __cplusplus
extern "C" {
#endif
*/
/*
void checkForExecution(struct Thread *threadToCheck, short accuracy);
struct Thread createThread(unsigned long execInterval, void newTask());
*/
/*
#ifdef __cplusplus
}
#endif
*/

class Thread {
 public:
  Thread(unsigned long execInterval, void newTask());

  void checkForExecution(short accuracy);
  void setExecutionInterval(unsigned long execInterval);
  unsigned long getExecutionInterval();

 private:
  unsigned long executionInterval;
  unsigned long lastExecutionEnded;
  void (*task)();
};
