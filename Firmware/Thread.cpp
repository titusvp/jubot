#include "Thread.h"
#include "Arduino.h"
/*
void checkForExecution(struct Thread *threadToCheck, short accuracy) {
  long check = (millis() - threadToCheck->lastExecutionEnded - threadToCheck->executionInterval);
  if (check >= -accuracy) {
    if (check < 0) {
      delay(-check);
    }
    threadToCheck->task();
    threadToCheck->lastExecutionEnded = millis();
  }
}

struct Thread createThread(unsigned long execInterval, void newTask()) {
  struct Thread newThread;
  newThread.lastExecutionEnded = 0;
  newThread.executionInterval = execInterval;
  newThread.task = newTask;
  return newThread;
}
*/
Thread::Thread(unsigned long execInterval, void newTask()) {
  executionInterval = execInterval;
  lastExecutionEnded = 0;
  task = newTask;
}

void Thread::checkForExecution(short accuracy) {
  long check = (millis() - lastExecutionEnded - executionInterval);
  if (check >= -accuracy) {
    if (check < 0) {
      delay(-check);
    }
    task();
    lastExecutionEnded = millis();
  }
}

void Thread::setExecutionInterval(unsigned long execInterval) {
  executionInterval = execInterval;
}

unsigned long Thread::getExecutionInterval() {
  return executionInterval;
}

