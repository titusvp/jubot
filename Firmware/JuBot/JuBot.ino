#include <nRF5x_BLE_API.h>
#include "Adafruit_TLC59711.h"


#define DEVICE_NAME "JuBot_TPU"

//activates the board
//must be set to HIGH prior to any any interaction on the spi or i2c bus
//otherwise the tlc and/or the bme can be damaged
#define VCC_ON A3

#define CHANNELS_PER_BOARD 12


//pins for spi bus - connected to tlc
#define dataPin   A4
#define clockPin  A5

//number of tlcs !has to be set
#define NUM_TLC59711 4
//number of motos per tlc board !has to be set
#define NUMBER_OF_MOTORS 2

#define MAX_FREQUENCY 21


//activates debugging on serial port
//will lead to nonstable readings for the bme sensor
//for debugging the bme sensor use a ble connection
#define DEBUG 0

//use software spi for tlc writings
Adafruit_TLC59711 tlc = Adafruit_TLC59711(NUM_TLC59711, clockPin, dataPin);

BLE ble;

uint8_t write_value[NUM_TLC59711] = {0};
uint8_t count_value[1] = {NUM_TLC59711};
uint8_t freq_value[1] = {MAX_FREQUENCY};

Ticker ticker_task1;

static const uint8_t writeService_uuid[]           = {0x71, 0x3D, 0, 0, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t motor_count_uuid[]            = {0x71, 0x3D, 0, 1, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t frequency_uuid[]              = {0x71, 0x3D, 0, 2, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t gatt_write_uuid[]             = {0x71, 0x3D, 0, 3, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};

uint8_t zero[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t current[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

GattCharacteristic writeCharacteristic(gatt_write_uuid, write_value, 1, NUMBER_OF_MOTORS*NUM_TLC59711, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ | GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE | GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE_WITHOUT_RESPONSE);
GattCharacteristic countCharacteristic(motor_count_uuid, count_value, 1, 1, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ);
GattCharacteristic freqCharacteristic(frequency_uuid, freq_value, 1, 1, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ);

//GattCharacteristic *allChars[] = {&writeCharacteristic, &countCharacteristic, &freqCharacteristic};  // commented out, because only writeCharacteristic will be needed for JuBot -> saves overhead and complexity
GattCharacteristic *allChars[] = {&writeCharacteristic};
GattService         writeService(writeService_uuid, allChars, sizeof(allChars) / sizeof(GattCharacteristic *));

void allOff();
//ble callbacks
void disconnectionCallBack(const Gap::DisconnectionCallbackParams_t *params) {
  allOff();
  writeWithoutSpinOn(); 
  
  Serial.println("- DISCONNECTED -");
  ble.startAdvertising();
}

void connectionCallBack(const Gap::ConnectionCallbackParams_t *params) { 
  Serial.println("- CONNECTED -");
}


void gattServerWriteCallBack(const GattWriteCallbackParams *Handler) {
  uint8_t buf[NUM_TLC59711*NUMBER_OF_MOTORS];
  uint16_t bytesRead;

  Serial.print("Trying to write... ");
  
  delay(15);
  
  if (Handler->handle == writeCharacteristic.getValueAttribute().getHandle()) {
    ble.readCharacteristicValue(writeCharacteristic.getValueAttribute().getHandle(), buf, &bytesRead);

  
    for(int i = 0; i < bytesRead; i++){
      current[i] = buf[i];
      Serial.print("0x");
      Serial.print(buf[i], HEX);
      Serial.print(" ");
    }

     Serial.println(" - Written!");

     writeWithoutSpinOn();
  }
}

void writeWithoutSpinOn(){




int counter = 0;
  for(int channel = 0; channel < NUM_TLC59711 * CHANNELS_PER_BOARD; channel++){
      
      if (channel % CHANNELS_PER_BOARD == CHANNELS_PER_BOARD-1){
        counter++;
      }
      if(channel >= 3 +(CHANNELS_PER_BOARD * counter) && channel <= (5 + CHANNELS_PER_BOARD * counter)){       
        tlc.setPWM(channel, (((current[NUMBER_OF_MOTORS * counter]+current[(NUMBER_OF_MOTORS * counter+1)])/2) / 255.0) * 65535); // set led so that it turns on when at least one motor on that board is active
      }
      else if(channel >= 6 +(CHANNELS_PER_BOARD * counter) && channel <= (8 + CHANNELS_PER_BOARD * counter)){ // set first motor
        tlc.setPWM(channel, (current[NUMBER_OF_MOTORS * counter] / 255.0) * 65535);
      }
      else if(channel >= 9 +(CHANNELS_PER_BOARD * counter) && channel <= (11 + CHANNELS_PER_BOARD * counter)){ // set second motor
        tlc.setPWM(channel, (current[NUMBER_OF_MOTORS * counter+1] / 255.0) * 65535);
      }
  }

  tlc.write(); 
  Serial.println("Updating...");
}

void periodicCallback() {
  if (ble.getGapState().connected) {
    writeWithoutSpinOn();
  }
}




void setup() {
  Serial.begin(9600);
  Serial.println("Vibration Demo");
  
  pinMode(VCC_ON, OUTPUT);
  digitalWrite(VCC_ON, HIGH);
  
  tlc.begin();
  tlc.write();

  //ticker_task1.attach(periodicCallback, 0.1);
  
  ble.init();
  ble.onDisconnection(disconnectionCallBack);
  ble.onConnection(connectionCallBack);
  ble.onDataWritten(gattServerWriteCallBack);
  
  ble.accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);
  ble.accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME));
  ble.accumulateAdvertisingPayload(GapAdvertisingData::SHORTENED_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME));

  ble.addService(writeService);

  ble.setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);


  ble.setDeviceName((const uint8_t *)DEVICE_NAME);
  ble.setTxPower(4);
  ble.setAdvertisingInterval(160);
  ble.setAdvertisingTimeout(0);

  ble.startAdvertising();
}

void loop() {
  ble.waitForEvent();
}

void allOff() {
  for (int i = 0; i < NUM_TLC59711* NUMBER_OF_MOTORS; i++){
    current[i] = 0x00;
  }
  
  ble.updateCharacteristicValue(writeCharacteristic.getValueAttribute().getHandle(), zero, NUM_TLC59711);
  Serial.println("Turning all off...");
}
