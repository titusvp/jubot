Um mit der Testfirmware euer Board zu testen nutzt ihr am besten eine App die BLE Services lesen und beschreiben kann. Wir nutzen den "BLE Scanner" dafür. Es gibt aber zahlreiche Apps im Appstore/GooglePlay die das können. 
Sucht mit der App das "TECO Wearable 007" und verbindet euch(Einschalten nicht vergessen). Als nächstes beschreibt ihr die Charakteristik mit bspw.: "FFFF" um so in der Firmware definierte Channel der Schieberegister anzusprechen
und bei standardmäßig 2 Motoren um beide auf voller Kraft anzusteuern.
(00FF) um nur den ersten von beiden anzusteueren usw. - gezählt wird von hinten wenn die Beschaltung der Dokumentation berücksichtigt wird.

Eure Anzahl von TLC Boards müsst ihr in der Konstanten :"#define NUM_TLC59711 2" definieren.

Viel Erfolg!

 
